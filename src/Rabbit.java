public class Rabbit {
	
	// -----------
	// PROPERTIES
	// -----------
	private int xPosition;
	private int yPosition;
	
	
	// -----------
	// CONSTRUCTOR 
	// ------------
	public Rabbit(int x, int y) {
		this.xPosition = x;
		this.yPosition = y;

	}
	
	// -----------
	// METHODS 
	// ------------
	public void printCurrentPosition(int x) {
	
		System.out.println("The current position of the rabbit is: "+ x);
		
		
	}

	public void sayHello() {
		System.out.println("Hello! I am a rabbit!");
	}
	
	// ----------------
	// ACCESSOR METHODS
	// ----------------
	
	// Put all your accessor methods in this section.
	
	public int getxPosition() {

		return xPosition;
		
	}

	public void setxPosition(int xPosition) {
		this.xPosition = xPosition;
	}

	public int getyPosition() {
		return yPosition;
	}

	public void setyPosition(int yPosition) {
		this.yPosition = yPosition;
	}
	
}