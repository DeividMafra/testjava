import com.sun.javafx.scene.layout.region.Margins.Converter;

public class SubstringForKids {

	public static void main(String[] args) {
		System.out.println("SUBSTRING FOR KIDS");
		System.out.println(substringForKids(-1,3,"apple"));
	}
	
	public static String substringForKids(int i, int j, String sentence) {
		String result = "";
		if(i<0)
			return "You gave me invalid numbers!";
		if (i==j) {
			 result = Character.toString(sentence.charAt(i));
			return result;	
		}else if(i<j) {
			for (int k = i; k <=j; k++) {
				result += Character.toString(sentence.charAt(k));
			}
			return result;
		}else if(i>j) {
			return "You gave me invalid numbers!";	
		}else 		
			return result;
	}
}